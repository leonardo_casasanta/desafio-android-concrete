package lvc.com.challengegithub;

import android.app.Application;

import com.squareup.leakcanary.LeakCanary;

import dagger.internal.DaggerCollections;
import lvc.com.challengegithub.di.ApplicationComponent;
import lvc.com.challengegithub.di.ApplicationModule;
import lvc.com.challengegithub.di.DaggerApplicationComponent;
import lvc.com.challengegithub.di.DatabaseModule;
import lvc.com.challengegithub.di.RepoModule;
import lvc.com.challengegithub.di.ServiceModule;

/**
 * Created by leonardo2050 on 05/02/18.
 */

public class GithubApplication extends Application {

    ApplicationComponent component;

    @Override
    public void onCreate() {
        super.onCreate();

        component = DaggerApplicationComponent.builder()
                .applicationModule(new ApplicationModule(this))
                .serviceModule(new ServiceModule())
                .databaseModule(new DatabaseModule())
                .repoModule(new RepoModule())
                .build();

        if (LeakCanary.isInAnalyzerProcess(this)) {
            return;
        }
        LeakCanary.install(this);
    }


    public ApplicationComponent getComponent() {
        return component;
    }

    public void setComponent(ApplicationComponent component) {
        this.component = component;
    }
}
