package lvc.com.challengegithub.database;

import android.arch.lifecycle.LiveData;
import android.arch.persistence.room.Dao;
import android.arch.persistence.room.Insert;
import android.arch.persistence.room.Query;

import java.util.List;

import lvc.com.challengegithub.model.PullRequest;
import lvc.com.challengegithub.model.PullRequestSearch;
import lvc.com.challengegithub.model.RepositoryGit;
import lvc.com.challengegithub.model.RepositoryGitSearch;

import static android.arch.persistence.room.OnConflictStrategy.REPLACE;

/**
 * Created by leonardo2050 on 05/02/18.
 */

@Dao
public interface PullRequestDao {

    @Insert(onConflict = REPLACE)
    void insert(PullRequest pullRequest);

    @Insert(onConflict = REPLACE)
    void insert(List<PullRequest> pullRequest);

    @Insert(onConflict = REPLACE)
    void insert(PullRequestSearch pullRequest);

    @Query("SELECT * FROM pullrequestsearch WHERE owner = :user AND repository = :repository")
    LiveData<PullRequestSearch> getPullRequestSearch(String user, String repository);

    @Query("SELECT * FROM pullrequest WHERE id in (:ids)")
    LiveData<List<PullRequest>> getPullRequestByIds(List<Integer> ids);

}
