package lvc.com.challengegithub.database;

import android.arch.lifecycle.LiveData;
import android.arch.persistence.room.Dao;
import android.arch.persistence.room.Insert;
import android.arch.persistence.room.Query;

import java.util.List;

import lvc.com.challengegithub.model.RepositoryGit;
import lvc.com.challengegithub.model.RepositoryGitSearch;

import static android.arch.persistence.room.OnConflictStrategy.REPLACE;

/**
 * Created by leonardo2050 on 05/02/18.
 */

@Dao
public interface RepositoryGitDao {

    @Insert(onConflict = REPLACE)
    void insert(RepositoryGit repositoryGit);

    @Insert(onConflict = REPLACE)
    void insert(List<RepositoryGit> repositoryGit);

    @Insert(onConflict = REPLACE)
    void insert(RepositoryGitSearch repositoryGitSearch);

    @Query("SELECT * FROM repositorygit")
    LiveData<List<RepositoryGit>> getAllRepositoryGit();

    @Query("SELECT * FROM repositorygit WHERE id in (:ids) ORDER BY starCount DESC")
    LiveData<List<RepositoryGit>> getRepositoryByIds(List<Integer> ids);

    @Query("SELECT * FROM repositorygitsearch WHERE querySearch = :query")
    LiveData<RepositoryGitSearch> getRepositorySearch(String query);

    @Query("SELECT * FROM repositorygitsearch WHERE querySearch = :query")
    RepositoryGitSearch getRepositorySearchDirect(String query);

}
