package lvc.com.challengegithub.database;

import android.arch.persistence.room.Database;
import android.arch.persistence.room.RoomDatabase;
import android.arch.persistence.room.TypeConverters;

import lvc.com.challengegithub.model.PullRequest;
import lvc.com.challengegithub.model.PullRequestSearch;
import lvc.com.challengegithub.model.RepositoryGit;
import lvc.com.challengegithub.model.RepositoryGitSearch;

/**
 * Created by leonardo2050 on 05/02/18.
 */

@Database(entities = {RepositoryGit.class, PullRequest.class, RepositoryGitSearch.class, PullRequestSearch.class}, version = 3)
@TypeConverters({RoomTypeConverters.class})
public abstract class GithubDatabase extends RoomDatabase {

    public abstract RepositoryGitDao repositoryGitDao();

    public abstract PullRequestDao pullRequestDao();

}
