package lvc.com.challengegithub.service;

import android.arch.lifecycle.LiveData;

import java.util.List;

import lvc.com.challengegithub.model.PullCallResponse;
import lvc.com.challengegithub.model.PullRequest;
import lvc.com.challengegithub.model.RepositoryGit;
import lvc.com.challengegithub.model.RepositoryCallResponse;
import retrofit2.Call;
import retrofit2.http.GET;
import retrofit2.http.Path;
import retrofit2.http.Query;

/**
 * Created by leonardo2050 on 23/02/17.
 */

public interface GithubService {

    @GET("users/{user}/repos")
    LiveData<ApiResponse<List<RepositoryGit>>> listRepos(@Path("user") String user);

    @GET("search/repositories")
    LiveData<ApiResponse<RepositoryCallResponse>> listRepos(@Query("q") String language, @Query("sort") String sort, @Query("page") int page);

    @GET("search/repositories")
    Call<RepositoryCallResponse> listReposReturningCall(@Query("q") String language, @Query("sort") String sort, @Query("page") int page);

    @GET("search/repositories")
    LiveData<ApiResponse<RepositoryCallResponse>> listRepos(@Query("q") String language, @Query("sort") String sort);

    @GET("repos/{user}/{repository}/pulls")
    LiveData<ApiResponse<List<PullRequest>>> listPulls(@Path("user") String user, @Path("repository") String repository);

}
