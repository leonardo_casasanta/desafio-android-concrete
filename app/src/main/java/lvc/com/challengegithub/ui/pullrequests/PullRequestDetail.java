package lvc.com.challengegithub.ui.pullrequests;

import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import android.widget.ProgressBar;

import java.lang.ref.WeakReference;

import lvc.com.challengegithub.R;
import lvc.com.challengegithub.model.PullRequest;

/**
 * Created by leonardo2050 on 05/02/18.
 */

public class PullRequestDetail extends Fragment {

    public static final String PULL_BUNDLE_NAME = "pull_extra_arg";

    public static PullRequestDetail getInstance(PullRequest pullRequest) {
        Bundle args = new Bundle();
        args.putParcelable(PULL_BUNDLE_NAME, pullRequest);

        PullRequestDetail pullRequestDetail = new PullRequestDetail();
        pullRequestDetail.setArguments(args);

        return pullRequestDetail;
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.pull_request_detail, container, false);

        WebView webView =   view.findViewById(R.id.web_view);
        ProgressBar progressBar =  view.findViewById(R.id.progress_bar);


        Bundle bundle = getArguments();
        PullRequest pull = bundle.getParcelable(PULL_BUNDLE_NAME);
        String url = pull.getUrl();

        progressBar.setVisibility(View.VISIBLE);
        webView.loadUrl(url);
        webView.setWebViewClient(new MyWebViewClient(progressBar));

        return view;
    }

    private static class MyWebViewClient extends WebViewClient {

        private WeakReference<ProgressBar> progressBarWeakReference;

        public MyWebViewClient(ProgressBar progressBar) {
            this.progressBarWeakReference = new WeakReference<ProgressBar>(progressBar);
        }

        @Override
        public void onPageFinished(WebView view, String url) {
            super.onPageFinished(view, url);
            ProgressBar progressBar = progressBarWeakReference.get();
            if (progressBar != null) {
                progressBar.setVisibility(View.GONE);
            }
        }

    }

}
