package lvc.com.challengegithub.ui.pullrequests;

import android.arch.lifecycle.ViewModelProviders;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ProgressBar;

import com.bumptech.glide.Glide;

import lvc.com.challengegithub.GithubApplication;
import lvc.com.challengegithub.R;
import lvc.com.challengegithub.model.Data;
import lvc.com.challengegithub.model.PullRequest;
import lvc.com.challengegithub.model.RepositoryGit;
import lvc.com.challengegithub.ui.MainActivity;
import lvc.com.challengegithub.ui.OnItemClick;
import lvc.com.challengegithub.ui.repositories.ListRepositoriesViewModel;
import lvc.com.challengegithub.ui.repositories.RepositoryAdapter;

/**
 * Created by leonardo2050 on 08/02/18.
 */

public class ListPullRequests extends Fragment {

    public static final String REPOSITORY_BUNDLE_NAME = "repository_git_extra_arg";

    PullRequestAdapter pullRequestAdapter;
    ListPullRequestsViewModel listPullRequestsViewModel;

    private ProgressBar progressBar;

    public static ListPullRequests getInstance(RepositoryGit repositoryGit) {
        Bundle args = new Bundle();
        args.putParcelable(REPOSITORY_BUNDLE_NAME, repositoryGit);

        ListPullRequests listPullRequests = new ListPullRequests();
        listPullRequests.setArguments(args);

        return listPullRequests;
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.default_list, container, false);
        RecyclerView recyclerView = initRecyclerView(view);
        pullRequestAdapter = initAdapter();
        recyclerView.setAdapter(pullRequestAdapter);

        progressBar = view.findViewById(R.id.progress_bar);

        return view;
    }

    private PullRequestAdapter initAdapter() {
         pullRequestAdapter = new PullRequestAdapter(Glide.with(this), new OnItemClick<PullRequest>() {
            @Override
            public void onClickItem(PullRequest data, int position) {
                PullRequestDetail pullRequestDetail = PullRequestDetail.getInstance(data);
                MainActivity mainActivity  = (MainActivity) getActivity();
                mainActivity.putPullDetailFragment(pullRequestDetail);
            }
        });

        return pullRequestAdapter;
    }

    private RecyclerView initRecyclerView(View view) {
        LinearLayoutManager layoutManager = new LinearLayoutManager(getContext());
        RecyclerView recyclerView =  view.findViewById(R.id.recycler_view);
        recyclerView.setLayoutManager(layoutManager);

        return recyclerView;
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        listPullRequestsViewModel = ViewModelProviders.of(this).get(ListPullRequestsViewModel.class);

        GithubApplication githubApplication = (GithubApplication) getActivity().getApplication();
        githubApplication.getComponent().inject(listPullRequestsViewModel);

        listPullRequestsViewModel.getPullRequestsData().observe(this, data -> {
            if(data.getStatus() == Data.Status.LOADING) {
                progressBar.setVisibility(View.VISIBLE);
            } else if (data.getStatus() == Data.Status.SUCCESS) {
                progressBar.setVisibility(View.INVISIBLE);
                pullRequestAdapter.setNewPullRequests(data.getData());
            }
        });
        RepositoryGit repositoryGit = getArguments().getParcelable(REPOSITORY_BUNDLE_NAME);
        listPullRequestsViewModel.search(repositoryGit.getOwner().getLogin(), repositoryGit.getName());
    }



}
