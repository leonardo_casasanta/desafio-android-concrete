package lvc.com.challengegithub.ui.repositories;

import android.arch.lifecycle.ViewModelProviders;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ProgressBar;
import android.widget.Toast;

import com.bumptech.glide.Glide;

import lvc.com.challengegithub.GithubApplication;
import lvc.com.challengegithub.R;
import lvc.com.challengegithub.model.Data;
import lvc.com.challengegithub.model.RepositoryGit;
import lvc.com.challengegithub.repository.LoadMoreState;
import lvc.com.challengegithub.ui.MainActivity;
import lvc.com.challengegithub.ui.OnItemClick;
import lvc.com.challengegithub.ui.pullrequests.ListPullRequests;

/**
 * Created by leonardo2050 on 05/02/18.
 */

public class ListRepositories extends Fragment {

    private ListRepositoriesViewModel listRepositoriesViewModel;
    private RepositoryAdapter repositoryAdapter;
    private ProgressBar progressBar;

    public static ListRepositories newInstance() {
        return new ListRepositories();
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.default_list, container, false);
        progressBar = view.findViewById(R.id.progress_bar);
        RecyclerView recyclerView = initRecyclerView(view);
        repositoryAdapter = initAdapter();
        recyclerView.setAdapter(repositoryAdapter);

        return view;
    }

    private RepositoryAdapter initAdapter() {
        RepositoryAdapter repositoryAdapter = new RepositoryAdapter(Glide.with(this), new OnItemClick<RepositoryGit>() {
            @Override
            public void onClickItem(RepositoryGit data, int position) {
                ListPullRequests listPullRequests = ListPullRequests.getInstance(data);
                MainActivity mainActivity = (MainActivity) getActivity();
                mainActivity.putListPullRequest(listPullRequests);
            }
        });

        return repositoryAdapter;
    }

    private RecyclerView initRecyclerView(View view) {
        LinearLayoutManager layoutManager = new LinearLayoutManager(getContext());
        RecyclerView recyclerView = view.findViewById(R.id.recycler_view);
        recyclerView.setLayoutManager(layoutManager);
        recyclerView.addOnScrollListener(new RecyclerView.OnScrollListener() {
            @Override
            public void onScrolled(RecyclerView recyclerView, int dx, int dy) {
                LinearLayoutManager layoutManager = (LinearLayoutManager) recyclerView.getLayoutManager();
                int lastPosition = layoutManager.findLastVisibleItemPosition();
                if (lastPosition == repositoryAdapter.getItemCount() - 1) {
                    listRepositoriesViewModel.loadNextPage();
                }
            }
        });

        return recyclerView;
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        listRepositoriesViewModel = ViewModelProviders.of(this).get(ListRepositoriesViewModel.class);

        GithubApplication githubApplication = (GithubApplication) getActivity().getApplication();
        githubApplication.getComponent().inject(listRepositoriesViewModel);

        listRepositoriesViewModel.getLoadMoreStateLiveData().observe(this, loadState -> {
            if (loadState == null) {
                setProgressBarVisible(false);
            } else {
                setProgressBarVisible(loadState.isRunning());
                showToastErrorIfNecessary(loadState);
            }
        });

        listRepositoriesViewModel.getRepositoryGitLiveData().observe(this, data -> {
            switch (data.getStatus()) {
                case SUCCESS:
                    setProgressBarVisible(false);
                    repositoryAdapter.setNewRepositories(data.getData());
                    break;

                case LOADING:
                    setProgressBarVisible(true);
                    break;

                case ERROR:
                    setProgressBarVisible(false);
                    break;
            }
        });

        listRepositoriesViewModel.requestNewSearch();
    }

    private void showToastErrorIfNecessary(LoadMoreState loadState) {
        String error = loadState.getErrorMessageIfNotHandled();
        if (error != null) {
            Toast.makeText(getContext(), error, Toast.LENGTH_LONG).show();
        }
    }

    private void setProgressBarVisible(boolean visible) {
        if (visible) {
            progressBar.setVisibility(View.VISIBLE);
        } else {
            progressBar.setVisibility(View.INVISIBLE);
        }
    }
}
