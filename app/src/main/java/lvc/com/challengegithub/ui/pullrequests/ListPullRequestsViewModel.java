package lvc.com.challengegithub.ui.pullrequests;

import android.arch.lifecycle.LiveData;
import android.arch.lifecycle.MutableLiveData;
import android.arch.lifecycle.Transformations;
import android.arch.lifecycle.ViewModel;

import java.util.List;

import javax.inject.Inject;

import lvc.com.challengegithub.model.Data;
import lvc.com.challengegithub.model.PullRequest;
import lvc.com.challengegithub.model.PullRequestSearch;
import lvc.com.challengegithub.repository.PullRequestRepo;

/**
 * Created by leonardo2050 on 08/02/18.
 */

public class ListPullRequestsViewModel extends ViewModel {

    @Inject
    PullRequestRepo pullRequestRepo;


    MutableLiveData<PullRequestSearch> input = new MutableLiveData<>();
    LiveData<Data<List<PullRequest>>> pullRequestsData = Transformations.switchMap(input, searchData -> {
        return pullRequestRepo.search(searchData.getOwner(), searchData.getRepository());
    });


    public void search(String owner, String repo) {
        input.setValue(new PullRequestSearch(owner, repo));
    }

    public LiveData<Data<List<PullRequest>>> getPullRequestsData() {
        return pullRequestsData;
    }
}
