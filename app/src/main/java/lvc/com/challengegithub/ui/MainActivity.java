package lvc.com.challengegithub.ui;

import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;

import lvc.com.challengegithub.R;
import lvc.com.challengegithub.ui.pullrequests.ListPullRequests;
import lvc.com.challengegithub.ui.pullrequests.PullRequestDetail;
import lvc.com.challengegithub.ui.repositories.ListRepositories;

public class MainActivity extends AppCompatActivity implements FragmentManager.OnBackStackChangedListener {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        if(savedInstanceState == null) {
            putListRepositories();
        }

        getSupportFragmentManager().addOnBackStackChangedListener(this);
        shouldDisplayHomeUp();
    }

    public void putListPullRequest(ListPullRequests fragment) {
        getSupportFragmentManager().beginTransaction().replace(R.id.fragment_container, fragment, "listPullRequestFragment").addToBackStack("listPulls").commit();
    }

    public void putPullDetailFragment(PullRequestDetail fragment) {
        getSupportFragmentManager().beginTransaction().replace(R.id.fragment_container, fragment, "detailsFragment").addToBackStack("details").commit();
    }

    public void putListRepositories() {
        Fragment fragment = ListRepositories.newInstance();
        getSupportFragmentManager().beginTransaction().add(R.id.fragment_container, fragment, "listRepos").commit();
    }

    @Override
    public void onBackStackChanged() {
        shouldDisplayHomeUp();
    }

    public void shouldDisplayHomeUp() {
        boolean canback = getSupportFragmentManager().getBackStackEntryCount() > 0;
        getSupportActionBar().setDisplayHomeAsUpEnabled(canback);
    }

    @Override
    public boolean onSupportNavigateUp() {
        //This method is called when the up button is pressed. Just the pop back stack.
        getSupportFragmentManager().popBackStack();
        return true;
    }

}
