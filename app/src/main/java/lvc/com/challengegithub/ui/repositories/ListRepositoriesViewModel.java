package lvc.com.challengegithub.ui.repositories;

import android.app.Application;
import android.arch.core.util.Function;
import android.arch.lifecycle.AndroidViewModel;
import android.arch.lifecycle.LiveData;
import android.arch.lifecycle.MutableLiveData;
import android.arch.lifecycle.Transformations;
import android.arch.lifecycle.ViewModel;
import android.support.annotation.NonNull;
import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

import javax.inject.Inject;
import lvc.com.challengegithub.model.Data;
import lvc.com.challengegithub.model.RepositoryGit;
import lvc.com.challengegithub.model.SearchParam;
import lvc.com.challengegithub.repository.GitRepositoryRepo;
import lvc.com.challengegithub.repository.LoadMoreState;
import lvc.com.challengegithub.repository.NextPageHandler;

/**
 * Created by leonardo2050 on 05/02/18.
 */

public class ListRepositoriesViewModel extends ViewModel {

    @Inject
    GitRepositoryRepo repoRepository;

    @Inject
    NextPageHandler nextPageHandler;

    private boolean searchStarted = false;


    private final MutableLiveData<SearchParam> input = new MutableLiveData<>();
    private final LiveData<Data<List<RepositoryGit>>> repositoryGitLiveData = Transformations.switchMap(input, new Function<SearchParam, LiveData<Data<List<RepositoryGit>>>>() {
        @Override
        public LiveData<Data<List<RepositoryGit>>> apply(SearchParam input) {
            return repoRepository.search(input);
        }
    });

    public void requestNewSearch() {
        if (!searchStarted) {
            searchStarted = true;
            input.setValue(SearchParam.crateDefaultQuery());
        }
    }

    public void loadNextPage() {
        SearchParam value = input.getValue();
        if (value == null) {
            return;
        }

        nextPageHandler.queryNextPage(value);
    }

    public LiveData<LoadMoreState> getLoadMoreStateLiveData() {
        return  nextPageHandler.getLoadMoreState();
    }

    public LiveData<Data<List<RepositoryGit>>> getRepositoryGitLiveData() {
        return repositoryGitLiveData;
    }

}
