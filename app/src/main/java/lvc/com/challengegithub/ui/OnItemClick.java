package lvc.com.challengegithub.ui;

/**
 * Created by leonardo2050 on 07/02/18.
 */

public interface OnItemClick<T> {

    void onClickItem(T data, int position);

}
