package lvc.com.challengegithub.ui.repositories;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.RequestManager;

import java.util.ArrayList;
import java.util.List;

import lvc.com.challengegithub.R;
import lvc.com.challengegithub.model.RepositoryGit;
import lvc.com.challengegithub.ui.OnItemClick;

/**
 * Created by leonardo2050 on 07/02/18.
 */

public class RepositoryAdapter extends RecyclerView.Adapter<RepositoryAdapter.RepositoryViewHolder> {

    private RequestManager glide;
    private List<RepositoryGit> repositories = new ArrayList<>();
    private OnItemClick<RepositoryGit> onItemClick;

    public RepositoryAdapter(RequestManager glide, OnItemClick<RepositoryGit> onItemClick) {
        this.glide = glide;
        this.onItemClick = onItemClick;
    }

    public void setNewRepositories(List<RepositoryGit> repositories) {
        Log.i("DATA", " RECEIVING NEW REPOSITORIES --> " + repositories.size());
        this.repositories = repositories;
        notifyDataSetChanged();
    }

    @Override
    public RepositoryViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext()).inflate(R.layout.repository_item, parent, false);
        return new RepositoryViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(RepositoryViewHolder viewHolder, final int position) {
        final RepositoryGit repository = repositories.get(position);


        glide.load(repository.getOwner().getAvatarURL()).into(viewHolder.imageViewUser);
        viewHolder.textViewRepositoryName.setText(repository.getName());
        viewHolder.textViewRepositoryDescription.setText(repository.getDescription());
        viewHolder.textViewRepositoryAuthor.setText(repository.getOwner().getLogin());
        viewHolder.textViewForks.setText(String.valueOf(repository.getForkCount()));
        viewHolder.textViewStars.setText(String.valueOf(repository.getStarCount()));

        viewHolder.buttonPullRequest.setOnClickListener( view  -> {
                if (onItemClick != null) {
                    onItemClick.onClickItem(repository, position);
                }
            });
    }

    @Override
    public int getItemCount() {
        return repositories.size();
    }

    static class RepositoryViewHolder extends RecyclerView.ViewHolder {

        private ImageView imageViewUser;
        private TextView textViewRepositoryName;
        private TextView textViewRepositoryDescription;
        private TextView textViewRepositoryAuthor;
        private TextView textViewStars;
        private TextView textViewForks;
        private Button buttonPullRequest;

        private RepositoryViewHolder(View itemView) {
            super(itemView);
            imageViewUser = itemView.findViewById(R.id.image_view_user);
            textViewRepositoryName = itemView.findViewById(R.id.text_view_repository_name);
            textViewRepositoryDescription =  itemView.findViewById(R.id.text_view_repository_description);
            textViewRepositoryAuthor = itemView.findViewById(R.id.text_view_author);
            textViewStars =  itemView.findViewById(R.id.text_view_star_count);
            textViewForks =  itemView.findViewById(R.id.text_view_fork_count);

            buttonPullRequest = itemView.findViewById(R.id.button_pull_request);
        }

    }

}
