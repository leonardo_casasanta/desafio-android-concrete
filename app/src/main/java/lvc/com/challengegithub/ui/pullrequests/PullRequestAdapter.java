package lvc.com.challengegithub.ui.pullrequests;

import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;
import com.bumptech.glide.RequestManager;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import lvc.com.challengegithub.R;
import lvc.com.challengegithub.model.PullRequest;
import lvc.com.challengegithub.ui.OnItemClick;


public class PullRequestAdapter extends RecyclerView.Adapter<PullRequestAdapter.RepositoryViewHolder> {

    private List<PullRequest> pullRequests = new ArrayList<>();
    private OnItemClick<PullRequest> onItemClick;
    private RequestManager glide;

    public PullRequestAdapter(RequestManager glide, OnItemClick<PullRequest> onItemClick) {
        this.glide = glide;
        this.onItemClick = onItemClick;
    }

    public void setNewPullRequests(List<PullRequest> pulls) {
        this.pullRequests = pulls;
        notifyDataSetChanged();
    }

    @Override
    public RepositoryViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext()).inflate(R.layout.pull_item, parent, false);
        return new RepositoryViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(RepositoryViewHolder viewHolder, final int position) {
        final PullRequest pull = pullRequests.get(position);

        glide.load(pull.getOwner().getAvatarURL()).into(viewHolder.imageViewUser);
        viewHolder.textViewRepositoryName.setText(pull.getTitle());
        viewHolder.textViewCreatedAt.setText(dateToString(pull.getCreatedAt()));
        viewHolder.textViewUpdatedAt.setText(dateToString(pull.getUpdatedAt()));
        viewHolder.textViewRepositoryAuthor.setText(pull.getOwner().getLogin());

        viewHolder.buttonDetails.setOnClickListener( view -> {
                if (onItemClick != null) {
                    onItemClick.onClickItem(pull, position);
                }

                /*
                Intent intent = new Intent(context, PullRequestDetail.class);
                Bundle bundle = new Bundle();
                bundle.putSerializable(PullRequest.PULL_BUNDLE_NAME, pull);
                intent.putExtras(bundle);
                context.startActivity(intent); */
        });
    }

    private String dateToString(Date date) {
        String formatedDate = SimpleDateFormat.getInstance().format(date);

        return formatedDate;
    }

    @Override
    public int getItemCount() {
        return pullRequests.size();
    }

    static class RepositoryViewHolder extends RecyclerView.ViewHolder {

        private ImageView imageViewUser;
        private TextView textViewRepositoryName;
        private TextView textViewCreatedAt;
        private TextView textViewUpdatedAt;
        private TextView textViewRepositoryAuthor;
        private Button buttonDetails;

        private RepositoryViewHolder(View itemView) {
            super(itemView);
            imageViewUser = itemView.findViewById(R.id.image_view_author);
            textViewRepositoryName = itemView.findViewById(R.id.text_view_pull_request_name);
            textViewCreatedAt =  itemView.findViewById(R.id.text_view_created_at);
            textViewUpdatedAt = itemView.findViewById(R.id.text_view_updated_at);
            textViewRepositoryAuthor = itemView.findViewById(R.id.text_view_author);
            buttonDetails =  itemView.findViewById(R.id.button_details);
        }

    }


}