package lvc.com.challengegithub.di;

import android.app.Application;
import android.arch.persistence.room.Room;
import android.content.Context;

import javax.inject.Singleton;

import dagger.Module;
import dagger.Provides;
import lvc.com.challengegithub.database.GithubDatabase;
import lvc.com.challengegithub.database.PullRequestDao;
import lvc.com.challengegithub.database.RepositoryGitDao;

/**
 * Created by leonardo2050 on 05/02/18.
 */
@Module
public class DatabaseModule {

    private static final String DATABASE_NAME = "github_database";

    @Provides
    @Singleton
    public GithubDatabase provideDatabase(Context context) {
        GithubDatabase githubDatabase = Room.databaseBuilder(context, GithubDatabase.class, DATABASE_NAME)
                .fallbackToDestructiveMigration()
                .build();

        return githubDatabase;
    }

    @Provides
    @Singleton
    public RepositoryGitDao provideRepositoryGitDao(GithubDatabase githubDatabase) {
        return githubDatabase.repositoryGitDao();
    }

    @Provides
    @Singleton
    public PullRequestDao providePullRequestDao(GithubDatabase githubDatabase) {
        return githubDatabase.pullRequestDao();
    }

}
