package lvc.com.challengegithub.di;

import javax.inject.Singleton;

import dagger.Component;
import lvc.com.challengegithub.GithubApplication;
import lvc.com.challengegithub.ui.MainActivity;
import lvc.com.challengegithub.ui.pullrequests.ListPullRequestsViewModel;
import lvc.com.challengegithub.ui.repositories.ListRepositoriesViewModel;

/**
 * Created by leonardo2050 on 05/02/18.
 */
@Singleton
@Component(modules = { ApplicationModule.class, ServiceModule.class, DatabaseModule.class, RepoModule.class})
public interface ApplicationComponent {

    void inject(GithubApplication target);

    void inject(ListRepositoriesViewModel viewModel);

    void inject(ListPullRequestsViewModel viewModel);

    void inject(MainActivity target);

}
