package lvc.com.challengegithub.di;

import android.app.Application;
import android.arch.persistence.room.Room;
import android.content.Context;

import javax.inject.Singleton;

import dagger.Module;
import dagger.Provides;
import lvc.com.challengegithub.database.GithubDatabase;
import lvc.com.challengegithub.database.PullRequestDao;
import lvc.com.challengegithub.database.RepositoryGitDao;
import lvc.com.challengegithub.repository.GitRepositoryRepo;
import lvc.com.challengegithub.repository.NextPageHandler;
import lvc.com.challengegithub.repository.PullRequestRepo;
import lvc.com.challengegithub.service.GithubService;

/**
 * Created by leonardo2050 on 08/02/18.
 */
@Module
public class RepoModule {

    @Provides
    @Singleton
    public GitRepositoryRepo provideGitRepositoryRepo(GithubDatabase githubDatabase, RepositoryGitDao repositoryGitDao, GithubService githubService) {
        GitRepositoryRepo gitRepositoryRepo = new GitRepositoryRepo(githubDatabase, repositoryGitDao, githubService);
        return gitRepositoryRepo;
    }

    @Provides
    @Singleton
    public PullRequestRepo providePullRequestRepo(GithubDatabase githubDatabase, PullRequestDao pullRequestDao, GithubService githubService) {
        PullRequestRepo pullRequestRepo = new PullRequestRepo(githubDatabase, pullRequestDao, githubService);
        return pullRequestRepo;
    }

    @Provides
    public NextPageHandler provideNextPageHandler(GitRepositoryRepo gitRepositoryRepo) {
        NextPageHandler nextPageHandler = new NextPageHandler(gitRepositoryRepo);
        return nextPageHandler;
    }

}
