package lvc.com.challengegithub.di;

import android.arch.persistence.room.PrimaryKey;

import javax.inject.Singleton;

import dagger.Module;
import dagger.Provides;
import lvc.com.challengegithub.service.GithubService;
import lvc.com.challengegithub.util.LiveDataCallAdapterFactory;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

/**
 * Created by leonardo2050 on 05/02/18.
 */

@Module
public class ServiceModule {

    private static final String BASE_URL = "https://api.github.com/";

    @Provides @Singleton
    public GithubService provideGithubService(Retrofit retrofit) {
        GithubService service = retrofit.create(GithubService.class);
        return  service;
    }

    @Provides @Singleton
    public Retrofit provideRetrofit() {
        Retrofit retrofit = new Retrofit.Builder()
                .baseUrl(BASE_URL)
                .addConverterFactory(GsonConverterFactory.create())
                .addCallAdapterFactory(new LiveDataCallAdapterFactory())
                .build();

        return retrofit;
    }

}
