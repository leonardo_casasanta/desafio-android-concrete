/*
 * Copyright (C) 2017 The Android Open Source Project
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package lvc.com.challengegithub.repository;

import android.arch.lifecycle.LiveData;
import android.arch.lifecycle.MutableLiveData;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import lvc.com.challengegithub.database.GithubDatabase;
import lvc.com.challengegithub.database.RepositoryGitDao;
import lvc.com.challengegithub.model.Data;
import lvc.com.challengegithub.model.RepositoryCallResponse;
import lvc.com.challengegithub.model.RepositoryGitSearch;
import lvc.com.challengegithub.model.SearchParam;
import lvc.com.challengegithub.service.ApiResponse;
import lvc.com.challengegithub.service.GithubService;
import retrofit2.Response;

public class FetchNextSearchPageTask implements Runnable {

    private final MutableLiveData<Data<Boolean>> liveData = new MutableLiveData<>();
    private final SearchParam searchParam;
    private final GithubService githubService;
    private final GithubDatabase githubDatabase;

    FetchNextSearchPageTask(SearchParam searchParam, GithubService githubService, GithubDatabase db) {
        this.searchParam = searchParam;
        this.githubService = githubService;
        this.githubDatabase = db;
    }

    @Override
    public void run() {
        RepositoryGitSearch current = githubDatabase.repositoryGitDao().getRepositorySearchDirect(searchParam.getQuery());
        if(current == null) {
            liveData.postValue(null);
            return;
        }

        final Integer nextPage = current.getNextPage();
        if (nextPage == null) {
            liveData.postValue(Data.success(false));
            return;
        }

       requestNewRepoData(nextPage, current);
    }

    private void requestNewRepoData(Integer nextPage, RepositoryGitSearch current) {
        try {
            Response<RepositoryCallResponse> response = githubService.listReposReturningCall(searchParam.getQuery(), searchParam.getOrderBy(), nextPage).execute();
            ApiResponse<RepositoryCallResponse> apiResponse = new ApiResponse<>(response);

            if (apiResponse.isSuccessful()) {
                mergeRepositoryData(apiResponse, current);
                liveData.postValue(Data.success(apiResponse.getNextPage() != null));
            } else {
                liveData.postValue(Data.error(apiResponse.errorMessage, true));
            }
        } catch (IOException e) {
            liveData.postValue(Data.error(e.getMessage(), true));
        }
    }

    private void mergeRepositoryData(ApiResponse<RepositoryCallResponse> apiResponse, RepositoryGitSearch current) {
        List<Integer> mergedRepoIds = new ArrayList<>();
        mergedRepoIds.addAll(current.getRepoIds());
        mergedRepoIds.addAll(apiResponse.body.getRepoIds());
        RepositoryGitSearch newSearch = new RepositoryGitSearch(searchParam.getQuery(), mergedRepoIds, apiResponse.getNextPage());
        try {
            githubDatabase.beginTransaction();
            RepositoryGitDao dao = githubDatabase.repositoryGitDao();
            dao.insert(newSearch);
            dao.insert(apiResponse.body.getItems());
            githubDatabase.setTransactionSuccessful();
        } finally {
            githubDatabase.endTransaction();
        }
    }

    LiveData<Data<Boolean>> getLiveData() {
        return liveData;
    }
}
