package lvc.com.challengegithub.repository;

import android.arch.lifecycle.LiveData;
import android.arch.lifecycle.Transformations;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;

import java.util.ArrayList;
import java.util.List;

import javax.inject.Inject;

import lvc.com.challengegithub.database.GithubDatabase;
import lvc.com.challengegithub.database.PullRequestDao;
import lvc.com.challengegithub.database.RepositoryGitDao;
import lvc.com.challengegithub.model.Data;
import lvc.com.challengegithub.model.PullCallResponse;
import lvc.com.challengegithub.model.PullRequest;
import lvc.com.challengegithub.model.PullRequestSearch;
import lvc.com.challengegithub.model.RepositoryCallResponse;
import lvc.com.challengegithub.model.RepositoryGit;
import lvc.com.challengegithub.model.RepositoryGitSearch;
import lvc.com.challengegithub.service.ApiResponse;
import lvc.com.challengegithub.service.GithubService;
import lvc.com.challengegithub.util.AbsentLiveData;

/**
 * Created by leonardo2050 on 05/02/18.
 */

public class PullRequestRepo {

    private GithubDatabase githubDatabase;
    private PullRequestDao pullRequestDao;
    private GithubService githubService;

    @Inject
    public PullRequestRepo(GithubDatabase githubDatabase, PullRequestDao pullRequestDao, GithubService githubService) {
        this.githubDatabase = githubDatabase;
        this.pullRequestDao = pullRequestDao;
        this.githubService = githubService;
    }

    public LiveData<Data<List<PullRequest>>> search(final String user, final String repository) {
        return new NetworkBoundResource<List<PullRequest>, List<PullRequest> >() {

            @Override
            protected boolean shouldFetch(@Nullable List<PullRequest> data) {
                return data == null || data.isEmpty();
            }

            @Override
            protected void saveCallResult(@NonNull List<PullRequest> pullRequests) {
                try {
                    githubDatabase.beginTransaction();

                    PullRequestSearch search = new PullRequestSearch();
                    search.setIdsPR(pullRequestsToIds(pullRequests));
                    search.setOwner(user);
                    search.setRepository(repository);
                    pullRequestDao.insert(search);

                    pullRequestDao.insert(pullRequests);

                    githubDatabase.setTransactionSuccessful();
                } finally {
                    githubDatabase.endTransaction();
                }
            }

            private List<Integer> pullRequestsToIds(List<PullRequest> pullRequests) {
                List<Integer> ids = new ArrayList<>();
                for (PullRequest pullRequest : pullRequests) {
                    ids.add(pullRequest.getId());
                }

                return ids;
            }

            @NonNull
            @Override
            protected LiveData<List<PullRequest>> loadFromDb() {
                return Transformations.switchMap(pullRequestDao.getPullRequestSearch(user, repository), pullSearch -> {
                    if (pullSearch == null) {
                        return AbsentLiveData.create();
                    } else {
                        return pullRequestDao.getPullRequestByIds(pullSearch.getIdsPR());
                    }
                });
            }

            @NonNull
            @Override
            protected LiveData<ApiResponse<List<PullRequest>>> createCall() {
                return githubService.listPulls(user, repository);
            }
        }.asLiveData();

    }

}
