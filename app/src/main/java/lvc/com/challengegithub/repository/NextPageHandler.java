package lvc.com.challengegithub.repository;

/**
 * Created by leonardo2050 on 30/01/18.
 */

import android.arch.lifecycle.LiveData;
import android.arch.lifecycle.MutableLiveData;
import android.arch.lifecycle.Observer;
import android.support.annotation.Nullable;

import javax.inject.Inject;

import lvc.com.challengegithub.model.Data;
import lvc.com.challengegithub.model.SearchParam;
import lvc.com.challengegithub.util.Objects;


public class NextPageHandler implements Observer<Data<Boolean>> {

    @Nullable
    private LiveData<Data<Boolean>> nextPageLiveData;
    private final MutableLiveData<LoadMoreState> loadMoreState = new MutableLiveData<>();
    private SearchParam query;
    private final GitRepositoryRepo repository;

    private boolean hasMore;

    @Inject
    public NextPageHandler(GitRepositoryRepo repository) {
        this.repository = repository;
        reset();
    }

   public void queryNextPage(SearchParam query) {
        if (Objects.equals(this.query, query)) {
            return;
        }

        unregister();
        this.query = query;
        nextPageLiveData = repository.searchNextPage(query);
        loadMoreState.setValue(new LoadMoreState(true, null));
        nextPageLiveData.observeForever(this);
    }

    @Override
    public void onChanged(@Nullable Data<Boolean> result) {
        if (result == null) {
            reset();
            return;
        }

        switch (result.getStatus()) {
            case SUCCESS:
                hasMore = Boolean.TRUE.equals(result.getData());
                unregister();
                loadMoreState.setValue(new LoadMoreState(false, null));
                break;
            case ERROR:
                hasMore = true;
                unregister();
                loadMoreState.setValue(new LoadMoreState(false, result.getMessage()));
                break;
        }

    }

    private void unregister() {
        if (nextPageLiveData != null) {
            nextPageLiveData.removeObserver(this);
            nextPageLiveData = null;
            if (hasMore) {
                query = null;
            }
        }
    }

    protected void reset() {
        unregister();
        hasMore = true;
        loadMoreState.setValue(new LoadMoreState(false, null));
    }

    public MutableLiveData<LoadMoreState> getLoadMoreState() {
        return loadMoreState;
    }
}
