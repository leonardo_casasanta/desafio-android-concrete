package lvc.com.challengegithub.repository;

import android.arch.lifecycle.LiveData;
import android.arch.lifecycle.Transformations;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;

import java.util.ArrayList;
import java.util.List;

import javax.inject.Inject;
import javax.inject.Singleton;

import lvc.com.challengegithub.database.GithubDatabase;
import lvc.com.challengegithub.database.RepositoryGitDao;
import lvc.com.challengegithub.model.Data;
import lvc.com.challengegithub.model.RepositoryCallResponse;
import lvc.com.challengegithub.model.RepositoryGit;
import lvc.com.challengegithub.model.RepositoryGitSearch;
import lvc.com.challengegithub.model.SearchParam;
import lvc.com.challengegithub.service.ApiResponse;
import lvc.com.challengegithub.service.GithubService;
import lvc.com.challengegithub.util.AbsentLiveData;
import lvc.com.challengegithub.util.AppExecutors;

/**
 * Created by leonardo2050 on 05/02/18.
 */
@Singleton
public class GitRepositoryRepo {

    private GithubDatabase githubDatabase;
    private RepositoryGitDao repositoryGitDao;
    private GithubService githubService;

    @Inject
    public GitRepositoryRepo(GithubDatabase githubDatabase, RepositoryGitDao repositoryGitDao, GithubService githubService) {
        this.githubDatabase = githubDatabase;
        this.repositoryGitDao = repositoryGitDao;
        this.githubService = githubService;
    }

    public LiveData<Data<Boolean>> searchNextPage(final SearchParam searchParam) {
        FetchNextSearchPageTask fetchNextSearchPageTask = new FetchNextSearchPageTask(searchParam, githubService, githubDatabase);
        AppExecutors.getInstance().networkIO().execute(fetchNextSearchPageTask);

        return fetchNextSearchPageTask.getLiveData();
    }

    public LiveData<Data<List<RepositoryGit>>> search(final SearchParam searchParam) {
        return new NetworkBoundResource<List<RepositoryGit>, RepositoryCallResponse>() {

            @Override
            protected boolean shouldFetch(@Nullable List<RepositoryGit> data) {
                return data == null || data.isEmpty();
            }

            @Override
            protected void saveCallResult(@NonNull RepositoryCallResponse item) {
                try {
                    githubDatabase.beginTransaction();

                    RepositoryGitSearch search = new RepositoryGitSearch();
                    List<RepositoryGit> repositoryGits = item.getItems();
                    search.setRepoIds(repositoriesToIds(repositoryGits));
                    search.setQuerySearch(searchParam.getQuery());
                    search.setNextPage(item.getNextPage());

                    repositoryGitDao.insert(search);

                    repositoryGitDao.insert(repositoryGits);

                    githubDatabase.setTransactionSuccessful();
                } finally {
                    githubDatabase.endTransaction();
                }
            }

            private List<Integer> repositoriesToIds(List<RepositoryGit> repositoryGits) {
                List<Integer> ids = new ArrayList<>();
                for (RepositoryGit repositoryGit : repositoryGits) {
                    ids.add(repositoryGit.getId());
                }

                return ids;
            }

            @NonNull
            @Override
            protected LiveData<List<RepositoryGit>> loadFromDb() {
                return Transformations.switchMap(repositoryGitDao.getRepositorySearch(searchParam.getQuery()), repoSearch -> {
                    if (repoSearch == null) {
                        return AbsentLiveData.create();
                    } else {
                        return repositoryGitDao.getRepositoryByIds(repoSearch.getRepoIds());
                    }
                });
            }

            @Override
            protected RepositoryCallResponse processResponse(ApiResponse<RepositoryCallResponse> response) {
                RepositoryCallResponse callResponse = response.body;
                if (callResponse != null) {
                    callResponse.setNextPage(response.getNextPage());
                }
                return callResponse;
            }

            @NonNull
            @Override
            protected LiveData<ApiResponse<RepositoryCallResponse>> createCall() {
                return githubService.listRepos(searchParam.getQuery(), searchParam.getOrderBy());
            }
        }.asLiveData();

    }


}
