package lvc.com.challengegithub.model;

import android.support.annotation.NonNull;

import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;
import java.util.List;

public class RepositoryCallResponse {

    @SerializedName("total_count")
    private int totalItens;
    private List<RepositoryGit> items;
    private Integer nextPage;

    public int getTotalItens() {
        return totalItens;
    }

    public void setTotalItens(int totalItens) {
        this.totalItens = totalItens;
    }

    public List<RepositoryGit> getItems() {
        return items;
    }

    public void setItems(List<RepositoryGit> items) {
        this.items = items;
    }

    public Integer getNextPage() {
        return nextPage;
    }

    public void setNextPage(Integer nextPage) {
        this.nextPage = nextPage;
    }

    @NonNull
    public List<Integer> getRepoIds() {
        List<Integer> repoIds = new ArrayList<>();
        for (RepositoryGit repo : items) {
            repoIds.add(repo.getId());
        }
        return repoIds;
    }
}
