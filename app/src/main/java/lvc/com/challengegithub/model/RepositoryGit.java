package lvc.com.challengegithub.model;


import android.arch.persistence.room.Embedded;
import android.arch.persistence.room.Entity;
import android.arch.persistence.room.PrimaryKey;
import android.os.Parcel;
import android.os.Parcelable;

import com.google.gson.annotations.SerializedName;

@Entity
public class RepositoryGit implements Comparable<RepositoryGit>, Parcelable {

    @PrimaryKey
    private int id;

    private String name;
    private String description;

    @SerializedName("stargazers_count")
    private int starCount;

    @SerializedName("forks_count")
    private int forkCount;

    @Embedded
    private Owner owner;

    public RepositoryGit() {
    }

    public RepositoryGit(int id, String name, String description, int starCount, int forkCount, Owner owner) {
        this.id = id;
        this.name = name;
        this.description = description;
        this.starCount = starCount;
        this.forkCount = forkCount;
        this.owner = owner;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public Owner getOwner() {
        return owner;
    }

    public void setOwner(Owner owner) {
        this.owner = owner;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getForkCount() {
        return forkCount;
    }

    public void setForkCount(int forkCount) {
        this.forkCount = forkCount;
    }

    public int getStarCount() {
        return starCount;
    }

    public void setStarCount(int starCount) {
        this.starCount = starCount;
    }

    @Override
    public int compareTo(RepositoryGit repository) {
        int compareToResult = name.compareTo(repository.getName());
        return compareToResult;
    }

    @Override
    public String toString() {
        return "RepositoryGit{" +
                "id=" + id +
                ", name='" + name + '\'' +
                ", description='" + description + '\'' +
                ", starCount=" + starCount +
                ", forkCount=" + forkCount +
                ", owner=" + owner +
                '}';
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeInt(this.id);
        dest.writeString(this.name);
        dest.writeString(this.description);
        dest.writeInt(this.starCount);
        dest.writeInt(this.forkCount);
        dest.writeParcelable(this.owner, flags);
    }

    protected RepositoryGit(Parcel in) {
        this.id = in.readInt();
        this.name = in.readString();
        this.description = in.readString();
        this.starCount = in.readInt();
        this.forkCount = in.readInt();
        this.owner = in.readParcelable(Owner.class.getClassLoader());
    }

    public static final Creator<RepositoryGit> CREATOR = new Creator<RepositoryGit>() {
        @Override
        public RepositoryGit createFromParcel(Parcel source) {
            return new RepositoryGit(source);
        }

        @Override
        public RepositoryGit[] newArray(int size) {
            return new RepositoryGit[size];
        }
    };
}
