package lvc.com.challengegithub.model;

import android.arch.persistence.room.Entity;
import android.arch.persistence.room.PrimaryKey;

import java.util.List;

/**
 * Created by leonardo2050 on 08/02/18.
 */
@Entity
public class PullRequestSearch {

    @PrimaryKey(autoGenerate = true)
    private int id;
    private String owner;
    private String repository;
    private List<Integer> idsPR;

    public PullRequestSearch() {
    }

    public PullRequestSearch(int id, String owner, String repository, List<Integer> idsPR) {
        this.id = id;
        this.owner = owner;
        this.repository = repository;
        this.idsPR = idsPR;
    }

    public PullRequestSearch(String owner, String repository) {
        this.owner = owner;
        this.repository = repository;
    }

    public List<Integer> getIdsPR() {
        return idsPR;
    }

    public void setIdsPR(List<Integer> idsPR) {
        this.idsPR = idsPR;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getOwner() {
        return owner;
    }

    public void setOwner(String owner) {
        this.owner = owner;
    }

    public String getRepository() {
        return repository;
    }

    public void setRepository(String repository) {
        this.repository = repository;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        PullRequestSearch that = (PullRequestSearch) o;

        if (owner != null ? !owner.equals(that.owner) : that.owner != null) return false;
        return repository != null ? repository.equals(that.repository) : that.repository == null;
    }

    @Override
    public int hashCode() {
        int result = owner != null ? owner.hashCode() : 0;
        result = 31 * result + (repository != null ? repository.hashCode() : 0);
        return result;
    }

    @Override
    public String toString() {
        return "PullRequestSearch{" +
                "id=" + id +
                ", owner='" + owner + '\'' +
                ", repository='" + repository + '\'' +
                '}';
    }
}
