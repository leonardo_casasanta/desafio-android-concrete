package lvc.com.challengegithub.model;

/**
 * Created by leonardo2050 on 08/02/18.
 */

public class SearchParam {

    public static final String SEARCH_LANGUAGE_DEFAULT = "language:java";
    public static final String SORTING_ARG_DEFAULT = "stars";

    private final String query;
    private final String orderBy;

    public SearchParam(String query, String orderBy) {
        this.query = query;
        this.orderBy = orderBy;
    }

    public static SearchParam crateDefaultQuery() {
        return new SearchParam(SEARCH_LANGUAGE_DEFAULT, SORTING_ARG_DEFAULT);
    }

    public String getQuery() {
        return query;
    }

    public String getOrderBy() {
        return orderBy;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        SearchParam that = (SearchParam) o;

        if (query != null ? !query.equals(that.query) : that.query != null) return false;
        return orderBy != null ? orderBy.equals(that.orderBy) : that.orderBy == null;
    }

    @Override
    public int hashCode() {
        int result = query != null ? query.hashCode() : 0;
        result = 31 * result + (orderBy != null ? orderBy.hashCode() : 0);
        return result;
    }

    @Override
    public String toString() {
        return "SearchParam{" +
                "query='" + query + '\'' +
                ", orderBy='" + orderBy + '\'' +
                '}';
    }
}
