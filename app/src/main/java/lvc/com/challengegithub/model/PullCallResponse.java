package lvc.com.challengegithub.model;

import java.util.List;

public class PullCallResponse {

    private List<PullRequest> pulls;

    public List<PullRequest> getPulls() {
        return pulls;
    }

    public void setPulls(List<PullRequest> pulls) {
        this.pulls = pulls;
    }
}
