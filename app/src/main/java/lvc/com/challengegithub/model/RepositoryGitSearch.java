package lvc.com.challengegithub.model;

import android.arch.persistence.room.Entity;
import android.arch.persistence.room.PrimaryKey;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;

import java.util.List;

/**
 * Created by leonardo2050 on 08/02/18.
 */
@Entity(primaryKeys = {"querySearch"})
public class RepositoryGitSearch {

    @NonNull
    private String querySearch;
    private List<Integer> repoIds;

    @Nullable
    private Integer nextPage;

    public RepositoryGitSearch() {
    }

    public RepositoryGitSearch(String querySearch, List<Integer> repoIds, Integer nextPage) {
        this.querySearch = querySearch;
        this.repoIds = repoIds;
        this.nextPage = nextPage;
    }

    public String getQuerySearch() {
        return querySearch;
    }

    public void setQuerySearch(String querySearch) {
        this.querySearch = querySearch;
    }

    public List<Integer> getRepoIds() {
        return repoIds;
    }

    public void setRepoIds(List<Integer> repoIds) {
        this.repoIds = repoIds;
    }

    public Integer getNextPage() {
        return nextPage;
    }

    public void setNextPage(Integer nextPage) {
        this.nextPage = nextPage;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        RepositoryGitSearch that = (RepositoryGitSearch) o;

        return querySearch != null ? querySearch.equals(that.querySearch) : that.querySearch == null;
    }

    @Override
    public int hashCode() {
        return querySearch != null ? querySearch.hashCode() : 0;
    }
}
