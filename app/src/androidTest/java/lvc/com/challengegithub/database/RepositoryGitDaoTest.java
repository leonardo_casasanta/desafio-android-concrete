package lvc.com.challengegithub.database;

import android.arch.lifecycle.LiveData;
import android.arch.persistence.room.Room;
import android.content.Context;
import android.support.test.InstrumentationRegistry;
import android.support.test.runner.AndroidJUnit4;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import lvc.com.challengegithub.model.Owner;
import lvc.com.challengegithub.model.RepositoryGit;
import lvc.com.challengegithub.model.RepositoryGitSearch;

import static android.support.test.espresso.matcher.ViewMatchers.assertThat;
import static lvc.com.challengegithub.util.LiveDataTestUtil.getValue;
import static org.hamcrest.Matchers.is;
import static org.hamcrest.core.IsNot.not;

/**
 * Created by leonardo2050 on 08/02/18.
 */
@RunWith(AndroidJUnit4.class)
public class RepositoryGitDaoTest {

     static final String SEARCH_LANGUAGE_DEFAULT = "language:java";

    private RepositoryGitDao repositoryGitDao;
    private GithubDatabase database;

    @Before
    public void createDb() {
        Context context = InstrumentationRegistry.getTargetContext();
        database = Room.inMemoryDatabaseBuilder(context, GithubDatabase.class).build();
        repositoryGitDao = database.repositoryGitDao();
    }

    @After
    public void closeDb() throws IOException {
        database.close();
    }

    @Test
    public void shouldSaveAndGetRepository() throws InterruptedException {
        List<RepositoryGit> repositoryGits = new ArrayList<>();
        repositoryGits.add(new RepositoryGit(1, "um", "Desc 1", 120, 310, new Owner("Leonardo", "http://exmoorpet.com/wp-content/uploads/2012/08/cat.png")));
        repositoryGits.add(new RepositoryGit(2, "dois", "Desc 2", 2, 2, new Owner("Leonardo", "http://exmoorpet.com/wp-content/uploads/2012/08/cat.png")));
        repositoryGits.add(new RepositoryGit(3, "tres", "Desc 3", 3, 3, new Owner("Leonardo", "http://exmoorpet.com/wp-content/uploads/2012/08/cat.png")));

        List<Integer> idsRepos = new ArrayList<>();
        for (RepositoryGit repositoryGit : repositoryGits) {
            idsRepos.add(repositoryGit.getId());
        }
        repositoryGitDao.insert(repositoryGits);

        RepositoryGitSearch repositoryGitSearch = new RepositoryGitSearch(SEARCH_LANGUAGE_DEFAULT, idsRepos, 2);
        repositoryGitDao.insert(repositoryGitSearch);

        RepositoryGitSearch searchLoaded = getValue(repositoryGitDao.getRepositorySearch(SEARCH_LANGUAGE_DEFAULT));
        assertThat(searchLoaded.getQuerySearch(), is(SEARCH_LANGUAGE_DEFAULT));
        assertThat(searchLoaded.getRepoIds().size(), is(3));
        assertThat(searchLoaded.getRepoIds().contains(1), is(true));
        assertThat(searchLoaded.getRepoIds().contains(2), is(true));
        assertThat(searchLoaded.getRepoIds().contains(3), is(true));

        LiveData<List<RepositoryGit>> repositoryGitLiveData = repositoryGitDao.getRepositoryByIds(searchLoaded.getRepoIds());
        List<RepositoryGit> loadedList = getValue(repositoryGitLiveData);
        assertThat(loadedList.size(), is(3));

        RepositoryGit loadedRepo = getRepositoryGit(loadedList, 1);
        assertThat(loadedRepo.getId(), is(1));
        assertThat(loadedRepo.getName(), is("um"));

        assertThat(loadedRepo.getDescription(), is("Desc 1"));
        assertThat(loadedRepo.getStarCount(), is(120));
        assertThat(loadedRepo.getForkCount(), is(310));
        assertThat(loadedRepo.getOwner().getLogin(), is("Leonardo"));
        assertThat(loadedRepo.getOwner().getAvatarURL(), is("http://exmoorpet.com/wp-content/uploads/2012/08/cat.png"));
    }

    private static RepositoryGit getRepositoryGit(List<RepositoryGit> loadedList, long id) {
        for (RepositoryGit repositoryGit : loadedList) {
            if (repositoryGit.getId() == id) {
                return repositoryGit;
            }
        }

        return null;
    }



}
